---
outline: deep
---

# CSS 样式

![css.png](/images/css/css.png)

## CSS 相关资料

::: tip 相关资源链接

- [iCSS - chokcoco](https://github.com/chokcoco/iCSS)
- [houdini.how](https://houdini.how/) /huːˈdiːni/
- [a-single-div - Lynn Fisher](https://github.com/lynnandtonic/a-single-div)  1 个 div，CSS 也可以实现炫酷效果（CSS drawings with only one HTML element.）
- [纯 CSS 图标库 ICON.CSS](https://github.com/picturepan2/icons.css) Single Element Pure CSS icons
:::

## CSS 书籍

- 《HTML5权威指南》 CSS 篇
- 《精通CSS》第三版

## 一点思考

### 为什么 element-plus 的边框不使用 border 而是使用 box-shadow

vue2 项目升级 vue3 时，使用了 element-plus 发现 输入框的边框，不是使用的 border，而是 box-shadow，为什么？

```css
/* https://element-plus.org/en-US/component/input.html#input */
.el-input__wrapper {
  box-shadow: 0 0 0 1px var(--el-input-border-color, var(--el-border-color)) inset;
}
```

网上找到了一篇文章

[Why we use the CSS property box-shadow instead of border in our app](https://threads.com/blog/34432545238/why-we-use-the-css-property-boxshadow-instead-of-border-in-our-app)

## flex 布局

### 固定高度 flex column 超出容器范围问题

父容器高度固定，宽度根据内容自适应。子元素从上到下，放不下就换行。如下图.

![flex-column-writing-mode-demo](/images/css/flex-column-writing-mode-demo.png)

代码如下，重点是需要用到 [writing-mode - mdn](https://developer.mozilla.org/zh-CN/docs/Web/CSS/writing-mode)

```md
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>flex-cloumn-固定高度超出容器问题</title>
    <style>
      .main {
        display: flex;
        flex-flow: wrap;
        max-height: 120px;
        margin-right: 20px;
        writing-mode: vertical-lr;
        border: 1px solid red;
      }
      .main .sec {
        writing-mode: horizontal-tb;
        border: 1px solid #ccc;
        padding: 10px;
        margin: 5px;
      }
      .flex {
        display: flex;
      }
    </style>
  </head>
  <body>
    <h2>max-height: 150px;</h2>
    <div class="flex">
      <div class="main">
        <div class="sec">123</div>
        <div class="sec">123</div>
        <div class="sec">123</div>
      </div>
      <div class="main">
        <div class="sec">123</div>
        <div class="sec">123</div>
        <div class="sec">123</div>
        <div class="sec">123</div>
        <div class="sec">123</div>
        <div class="sec">123</div>
      </div>
      <div class="main">
        <div class="sec">123</div>
        <div class="sec">123</div>
        <div class="sec">123</div>
        <div class="sec">123</div>
        <div class="sec">123</div>
        <div class="sec">123</div>
        <div class="sec">123</div>
        <div class="sec">123</div>
        <div class="sec">123</div>
      </div>
    </div>
  </body>
</html>
```

完整 demo 参见：[flex-cloumn-固定高度超出容器问题 - fedemo](<https://github.com/dev-zuo/fedemo/blob/8d01309fa2510d7bed9abb4254d0c42f5b64325a/src/DebugDemo/flex-cloumn-%E5%9B%BA%E5%AE%9A%E9%AB%98%E5%BA%A6%E8%B6%85%E5%87%BA%E5%AE%B9%E5%99%A8%E9%97%AE%E9%A2%98/index.html>)
