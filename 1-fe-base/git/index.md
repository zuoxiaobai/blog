# Git

## github push 经常失败的问题？

github 仓库在 git push 时，经常会出现 git push 失败的问题，log 如下

> github push OpenSSL SSL_read: Connection was reset, errno 10054

> fatal: unable to access 'xxx': Failed to connect to github.com port 443 after 21113 ms: Timed out

这种情况，可以运行如下命令，或者关掉梯子(代理)，再 push

```bash
git config --global http.sslVerify "false"
```

参考： [OpenSSL SSL_read: Connection was reset, errno 10054 #2251](https://github.com/Kong/insomnia/issues/2251)
