---
outline: deep
---

# HTML 语义化

## picture 元素使用场景

[Responsive images 自适应图片指南 - web.dev](https://web.dev/responsive-images/)

网页使用 `<picture>` 元素或 img 元素的 srcset 属性来指定自适应图片。但是，某些浏览器和抓取工具无法理解这些属性。 我们建议您一律通过 src 属性指定后备网址。

```html
<picture>
  <source media="(min-width: 800px)" srcset="head.jpg, head-2x.jpg 2x">
  <source media="(min-width: 450px)" srcset="head-small.jpg, head-small-2x.jpg 2x">
  <img src="head-fb.jpg" srcset="head-fb-2x.jpg 2x" alt="a head carved out of wood">
</picture>
```

借助 srcset 属性，您可指定同一图片的不同版本，特别是针对不同屏幕尺寸。

```html
<img
  srcset="maine-coon-nap-320w.jpg 320w, maine-coon-nap-480w.jpg 480w, maine-coon-nap-800w.jpg 800w"
  sizes="(max-width: 320px) 280px, (max-width: 480px) 440px, 800px"
  src="maine-coon-nap-800w.jpg"
  alt="A watercolor illustration of a maine coon napping leisurely in front of a fireplace">
```

参考：[Google 图片 SEO 最佳实践 - google](https://developers.google.com/search/docs/appearance/google-images?hl=zh-cn)
