---
outline: deep
---

# HTTP/前端后交互

## axios

[axios](https://github.com/axios/axios)

## http3

### 图解 QUIC 连接

[illustrate - github](https://github.com/cangSDARM/illustrate)，在线查看，图解 QUIC, TLS 1.2, TLS 1.3, DTLS 协议的连接及会话过程，[在线查看](https://cangsdarm.github.io/illustrate/)

对每一个字节的解释和再现，QUIC 是一个基于 UDP 的安全流协议，构成了 HTTP/3 的基础。在这个演示中，客户端通过 TLS 加密协商 QUIC 协议连接服务器。客户端发送"ping"、接收"pong"后终止连接。点击下面开始探索。
