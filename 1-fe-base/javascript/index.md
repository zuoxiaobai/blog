---
outline: deep
---

# JavaScript

## JavaScript 手写题

[js-challenges - github](https://github.com/Sunny-117/js-challenges)

- 实现 Promise.all
- JSON2DOM = react 的 render 函数
- 树形结构转成列表
- 列表转成树形结构
- Array.prototype.flat
- instanceof
- call apply bind
- Array.prototype.map
- 正则表达式模版字符串
- lodash.get
- 深拷贝
- 寄生组合式继承
- 发布订阅者模式
- 岛屿数量

## 工具库

### lodash-es

#### @types/lodash uniqBy第二参数必填导致的vscode报错问题

lodash 去重的函数 uniqBy 第二个参数文档里面是选填，但 js 项目升级为 ts 后，发现第二个参数为必填，和文档有冲突。

解决方法：仅有一个参数时，使用 uniq 即可

## 一些问题

### Map 使用时，注意数字字符串作为key的场景

如果 key 为 0，使用 "0" 字符串是拿不到数据的，注意这点和对象的行为不一样
