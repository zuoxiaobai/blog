# svelte 跨框架组件

> /svelt/

## 为什么需要用到 svelte?

假设你需要开发一个功能引导组件 sdk，但你需要兼容所有框架（Vue2/Vue3/React/Angular 等）

这种情况你会选用什么技术栈呢？原生 js + 直接操作 DOM？

显然这种场景对于目前大多习惯使用 MVVM 类型开发框架的开发人员来讲很不友好。

Svelte 框架正好可以解决这个问题，有兴趣可以看看 [功能引导 shepherd - github](https://github.com/shipshapecode/shepherd)

Svelte 将你的代码编译成体积小、不依赖框架的普通 JS 代码，让你的应用程序无论启动还是运行都变得迅速。
