---
outline: deep
---

# Vue

## vue3 怎么通过 ref 去拿 dom

vue3 使用 ref 代替 this.$refs 后，获取 dom 元素方式变更，例子如下

- vue 组件 xx.value.$el
- 普通 html 元素 xx.value

```html
<template>
    <MyComp ref="myCompRef"></MyComp>
    <div ref="divRef"></div>
</template>
<script lang="ts" setup>
import { ref } from 'vue';

const myCompRef = ref()
const mainRef = ref()
// 自定义组件 dom
myCompRef.value.$el.scrollTop
// 普通 html 元素，比如 div
divRef.value.scrollTop

</script>
```

## 一些思考

### why window.onerror not working vue？

为什么 window.onerror 捕获不到 vue 项目的错误。vue 内部捕获了，通过 Vue.config.errorHandler 统一处理

为什么要这么做？有什么好处？
