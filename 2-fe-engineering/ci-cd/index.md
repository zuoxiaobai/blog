---
outline: deep
---

# CI/CD DevOps

自动化构建、部署，可快速切换构建流程、版本、部署服务器等。可极大提升项目交付效率。

## 自动化部署目标

- [x] vitepress + github + 阿里云效 + 阿里云 oss，提交代码后一键部署
- [x] 多版本、多环境配置 - 静态文件部署版本(阿里云 oss)

  - 多版本时，不同的分支对应不同版本：其中主分支对应线上。其他分支以版本命名，比如 v1.0.0，v1.1.0
  - 多环境，以 阿里云 oss 为例，测试环境和正式环境使用不同的 bucket，f-zuo11-com 与 f-zuo11-com-test-env，再分别配置域名 CNAME 解析到不同的域名 f.zuo11.com（正式）、f-test.zuo11.com（测试环境）
  - 测试环境和正式环境使用两条流水线，正式环境只能部署 master，测试环境部署对应的版本分支。测试验证通过后，以提交 Pull Request 的方式合并代码（github/gitlab 页面操作，不要直接用工具和命令合并），合并完成后，即可部署 master 到主分支完成新功能上线
  - oss 注意事项：创建新的 Bucket 时，需要进行 必要的 3 步操作才能正常访问：1、在概览中找到该 oss bucket 的域名，添加 CNAME 域名解析到该地址 2、在 bucket 配置 - 域名管理中，绑定对应的域名 3、在数据管理 - 静态页面中配置默认首页为 index.html，勾选开通子目录首页(index)，这样才能访问目录时，自动解析到 index.html

- [x] 多版本、多环境（云服务器(ECS)版）- 支持 node.js 接口服务、ssr 服务端渲染、静态服务等
  - Koa.js + MongoDB 接口服务 https://config.zuo11.com/share/shortLink/list ，怎么做自动化部署？
- [ ] github actions 多版本、多服务器
  -
- [ ] gitlab 自动化部署
- [ ] jenkins
- [ ] docker
- [ ] nginx 配置
- [ ] 流水线（git 源、代码扫描/单测等、构建、部署）
- [ ] 不停机更新
- [ ] 服务降级
- [ ] 新上线功能出现异常后快速回退
- [ ] 灰度发布
- [ ] 一键扩容
- [ ] 服务压测、性能评估 （使用 JMeter 进行压力测试）
- [ ] 安全防护（DDos 工具防护、快速恢复、提升服务稳定性）
- [ ] 成本

## 基础概念

### 什么是 CI/CD？

CI（持续交付）是功能迭代后自动构建、打包、校验代码格式、跑单测、单测覆盖率，常见的就是 Webpack、Rollup、ESLint 等。

CD（持续部署）是经过 CI 后，代码自动部署到服务器。

也可参考 [持续集成是什么？ - 阮一峰](http://www.ruanyifeng.com/blog/2015/09/continuous-integration.html)

### 什么是 DevOps

DevOps 是 Development（开发） 和 Operations（运维） 的组合词，通过自动化 "软件交付" 和 "架构变更" 的流程，来使得构建、测试、发布软件能够更加地快捷、频繁和可靠。

> DevOps can be best explained as people working together to conceive, build and deliver secure software at top speed. DevOps practices enable software development (dev) and operations (ops) teams to accelerate delivery through automation, collaboration, fast feedback, and iterative improvement.

DevOps 可以最好地解释为人们一起工作以最快的速度构思、构建和交付安全软件。 DevOps 实践使软件开发 (dev) 和运营 (ops) 团队能够通过自动化、协作、快速反馈和迭代改进来加速交付。

参考：

- [万字长文带你彻底搞懂什么是 DevOps - 掘金](https://juejin.cn/post/6965860856311578637)
- [What is DevOps? | GitLab](https://about.gitlab.com/topics/devops/#the-four-phases-of-dev-ops)

## 比较好的 CI/CD 平台

### 阿里云-云效

[阿里云-云效](https://www.aliyun.com/product/yunxiao) 阿里云企业级一站式 DevOps，可以免费使用（会限制人数、流水线数量等，个人项目够用了）。相关文章 [CI 持续集成 - 阿里云云效](https://learnku.com/articles/13794/ci-continuous-integration-ali-cloud-effect)

#### 流水线

CICD 一般需要找到流水线设置，一般基础的分为 3 个阶段

1. 设置 git 源
2. 构建（比如前端项目执行 npm run build）、此时可以选择分支，是否执行 npm install
3. 部署（使用构建生成的文件部署）

![aliyun-cicd-oss-line.png](/images/cicd/aliyun-cicd-oss-line.png)

以自动化部署 github 仓库 vitepress 项目到阿里云 oss 为例，流水线运行逻辑

```bash
# 克隆代码
# 使用工作路径 /root/workspace/dev-zuo-blog
# [INFO] 清理文件夹 /root/workspace /root/workspace/dev-zuo-blog...
git clone https://github.com/dev-zuo/blog.git  --branch main /root/workspace/dev-zuo-blog
# 正克隆到 '/root/workspace/dev-zuo-blog'...
# [INFO]  cd /root/workspace/dev-zuo-blog
# [SUCCESS] 克隆成功
# [INFO] 获取到上一次运行commit ID 2268a51489cae3588d4b091734ba6f9cd8612379
# [18:55:16] ###commitId###:xx ###commitMsg###:chore: update git note
# 获取最近几次提交commit让你知道部署的是最新代码

# [18:55:26] [INFO] 执行用户命令
cnpm install
npm run build
# build log
# [18:55:46] build complete in 8.50s.

# [INFO] 使用工作路径/root/workspace/dev-zuo-blog
ossutil cp -r /root/workspace/dev-zuo-blog/.vitepress/dist oss://f-zuo11-com/ --update
# [18:55:52] [SUCCESS]上传成功
```

#### vitepress + oss 自动化部署

> 创建流水线后，运行该流水线，即可自动拉取 github 仓库最新代码，执行构建，并更新到阿里云 OSS

在 阿里云 - 云效 工作台，新建项目(DevOps)，切换到流水线 Tab

新建流水线，选择空模板，进入后选择 Node.js 技术栈，选择 部署到 oss

![aliyun-cicd-oss-ci.png](/images/cicd/aliyun-cicd-oss-ci.png)

配置构建流程（github 源建议选择香港，不然可能 git clone 拉取不下来）

![aliyun-cicd-oss.png](/images/cicd/aliyun-cicd-oss.png)

设置构建命令、构建生成文件目录，目标 oss

![aliyun-cicd-oss.png](/images/cicd/aliyun-cicd-oss-2.png)

创建完成后，每当有新提交的代码，想自动化部署，点击运行流水线，即可完成。

### gitlab CI/CD

[使用 GitLab CI/CD 和阿里云 CLI 自动部署前端项目 - 掘金](https://learnku.com/articles/13794/ci-continuous-integration-ali-cloud-effect)

### CircleCI

### Jenkins

## CI/CD 原理，手写实现

1、[Vue + Node.js 从 0 到 1 实现自动化部署工具 - 掘金](https://juejin.cn/post/7070921715492061214) `原创`

2、[dev-zuo/zuo-deploy](https://github.com/dev-zuo/zuo-deploy) `原创`，基于 Vue3 + Node.js 的轻量 Linux 操作面板，支持简单实时终端、自动化部署、持续集成、Nginx 配置管理/操作、Https 证书管理等。

## 相关技术人

### CookieBoty

系列文章 [从 0 到 1 node 项目管理系统：构建篇-jenkins - CookieBoty](https://juejin.cn/post/6864163222187540488)

掘金小册 [基于 Node 的 DevOps 实战](https://juejin.cn/book/6948353204648148995)
