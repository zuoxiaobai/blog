# 前端工程化

前端工程化，就是指对前端进行一些流程的标准化，让开发变得更有效率，且更好地做产品交付。

参考：[前端工程化指的是什么？](https://www.51cto.com/article/741101.html)

## 超详细的前端工程化入门教程

[超详细的前端工程化入门教程 - freecodecamp](https://www.freecodecamp.org/chinese/news/front-end-engineering-tutorial/)

- 技术选型
- 统一规范
- 测试
- 部署
- 监控
- 性能优化
- 重构
