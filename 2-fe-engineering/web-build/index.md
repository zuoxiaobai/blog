# 打包构建

## 以 vue-i18n 为例，实现 3 个打包 npm 包

假设你需要实现一个类似 vue-18n 的库，简单实现下面三个库，并理解为什么 vue-i18n 会实现下面三个打包类 npm 包，解决了什么问题。

- `@intlify/unplugin-vue-i18n`  支持 vite 和 webpack 的 unplugin 包
- `@intlify/vite-plugin-vue-i18n` vite 插件包
- `@intlify/vue-i18n-loader` webpack 的 loader 包

参考: [optimization - vue-i18n](https://vue-i18n.intlify.dev/guide/advanced/optimization.html)
