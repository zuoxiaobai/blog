---
outline: deep
---

# 可复用代码

快速搬砖常用代码

## 服务端

### koa+mongodb

[zuo-config-server](https://github.com/dev-zuo/zuo-config-server) jwt登录鉴权 + 允许跨域中间件 + mongodb 增删改查

### node小程序登录鉴权流程

[koa 小程序登录全流程接口 demo](https://github.com/dev-zuo/https-node-api-demo/blob/main/src/mpServer/index.js) 小程序 通过 node 请求微信接口用 appid+secret 获取 session_key + openid，jwt sign+验证

[小程序云开发收费后服务迁移方案](https://github.com/dev-zuo/mp-cloud-test/issues/4)

### SpringBoot Demo

[springboot-demo](https://github.com/dev-zuo/springboot-demo)，用 SpringBoot 写 GET/POST 接口 + 支持跨域注解

## vue

### 模板

- [vue3-base](https://github.com/dev-zuo/vue3-base) vue3 初始化项目，Vue3 + Vite + TypeScript + Vue Router + Pinia + Vitest + ESLint + Prettier
- [vue2-base](https://github.com/dev-zuo/vue2-base) TODO
- [vuepress-base](https://github.com/dev-zuo/vuepress-base) vuepress init project
- [vitepress-template](https://github.com/dev-zuo/vitepress-template) vitepress 空模板

### vue3+ts+vite+ElementPlus+sass+axios增删改查

[zuo-config-fe](https://github.com/dev-zuo/zuo-config-fe) 登录 + 全局路由拦截 + 增删改查

### vue2 echarts

[vue-chart](https://github.com/dev-zuo/vue-chart) vue2 echarts, 支持 resize 后重绘

### 列表、分页

### 弹窗

### 弹窗+表单

### 删除确认

## fe-framework-study

[fe-framework-study](https://github.com/dev-zuo/fe-framework-study)，用不同的前端开发框架开发同一个功能 10 次，框架入门练手小项目
