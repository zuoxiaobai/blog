# 低代码

## 海报图片编辑生成

[vue-fabric-editor - github](https://github.com/nihaojob/vue-fabric-editor)

基于 fabric.js 和 Vue 开发的图片编辑器，可自定义字体、素材、设计模板。

![img-editor-low-code.png](/images/lowcode/img-editor-low-code.png)

## 弹窗低代码

[我在淘宝做弹窗，2022 年初的回顾与展望 - 掘金](https://juejin.cn/post/7122797194519773221)

![taobao-dialog-low-code.webp](/images/lowcode/taobao-dialog-low-code.webp)
