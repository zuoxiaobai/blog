# 性能优化

[vue3 table 性能优化，减少 85% 渲染耗时](https://juejin.cn/post/7194516447932973112) `原创`

[Vue CLI 项目页面打开时间优化：从 16 秒到 2 秒内](http://www.zuo11.com/blog/2020/11/vue_cli_slow.html) `原创`
