---
outline: deep
---

# SEO搜索引擎优化

## SEO 教程

推荐教程：[SEO 新手指南 - Google 搜索中心 ](https://developers.google.com/search/docs?hl=zh-cn) 如下图，建议把整个文档完整的看一遍。

![google搜索中心seo新手指南.png](/images/seo/google搜索中心seo新手指南.png)

## google 站点链接

站点链接（sitelinks）是指来自同一网域并汇总到一条网页结果下的链接。 google 会分析网站的链接结构，以寻找可为用户节省时间的捷径，让用户快速找到需要的信息。


![google-sitelinks.png](/images/seo/google-sitelinks.png)

> 只有当我们认为站点链接将对用户有用时，才会在搜索结果中显示站点链接。如果我们的算法无法根据您的网站结构查找合适的站点链接，或者我们认为您网站的站点链接与用户的查询不相关，我们就不会显示这些站点链接。-- google

::: tip
- 1、该功能是 google 自动生成，无法配置 
- 2、只有当 google 认为站点连接对用户有用时，才会在搜索结果中显示
- 3、需要合理规划网站目录层级、且对应网页 title、description meta 配置合理，页面内容质量高。
::: 

### 站点链接最佳做法

> 目前，站点链接是自动生成的。我们一直在不断努力改进我们的站点链接算法，或许今后我们会采纳网站所有者的意见。不过，您可以遵循一些最佳做法，改善站点链接的质量。 -- google

- 确保您用作网页标题的文字信息丰富、相关且紧凑。
- 创建便于用户浏览的合理网站结构，并确保从其他相关网页链接到您的重要网页。
- 确保内部链接的定位文字简明扼要，且与其指向的网页相关。
- 避免内容重复。
- 如果您需要移除站点链接，请考虑从您的网站中移除该网页或使用 noindex。

## google search struct data

> 资料来源：google 搜索 google search struct data

google 搜索结构化数据是指在 google 搜索结果中展示富媒体搜索结果，而不是普通的单一连接、文本。它可以提升点击率、互动率等。

### 为什么要向网页添加结构化数据？

[Google 搜索中的结构化数据标记简介](https://developers.google.com/search/docs/appearance/structured-data/intro-structured-data?hl=zh-cn#structured-data)

添加结构化数据可让您获得对用户更有吸引力的搜索结果，并可能会鼓励用户与您的网站进行更多互动，这就是富媒体搜索结果。 以下是一些为网站实现了结构化数据的案例研究：

- Rotten Tomatoes 为 10 万个独特网页添加了结构化数据。衡量结果表明，采用结构化数据的网页在**点击率**方面比不含结构化数据的网页**高 25%**。
- Food Network 已将其 80% 的网页转换为启用搜索结果功能，结果**访问量增加了 35%**。
- Rakuten 发现，用户在已实现结构化数据的网页上花费的时间是在不含结构化数据的网页上的 1.5 倍，且在包含搜索结果功能的 AMP 网页上的**互动率**是在不含相应功能的 AMP 网页上的 3.6 倍。
- 雀巢公司经过衡量发现，在搜索结果中显示为富媒体搜索结果的网页所获的**点击率**比不显示为富媒体搜索结果的网页高 82%。


### 怎么配置页面在 google 搜索中富媒体显示

结构化数据在 Google 搜索中的运作方式是，根据页面中 html 文本中特定的配置信息，来决定对应搜索结果的展示效果

除非另有说明，否则 Google 搜索支持以下格式的结构化数据。一般而言，我们建议您使用最易于实现和维护的格式（在大多数情况下是 JSON-LD）；只要标记有效且已按照功能的说明文档正确实现，那么 3 种格式对 Google 来说都没问题。

| 格式 | 说明
--- | ---
JSON-LD*（推荐）	| 嵌入 HTML 网页 `<head>` 和 `<body>` 元素的 `<script>` 标记中的 JavaScript 表示法。此标记不与用户可见文本交错显示，使嵌套数据项更易于表达，例如，Event>MusicVenue>PostalAddress>Country。 此外，Google 可以读取通过 [JavaScript 代码或内容管理系统中的嵌入式微件等动态注入网页内容的 JSON-LD 数据](https://developers.google.com/search/docs/guides/generate-structured-data-with-javascript?hl=zh-cn)。
微数据	| 一种开放社区 HTML 规范，用于在 HTML 内容中嵌套结构化数据。与 RDFa 一样，它会使用 HTML 标记属性为您想以结构化数据形式显示的属性命名。它通常用在 `<body>` 元素中，但也可用在 `<head>` 元素中。
RDFa	| 一种 HTML5 扩展功能，通过引入与您要向搜索引擎描述的用户可见内容对应的 HTML 标记属性来支持关联的数据。RDFa 通常用在 HTML 网页的 `<head>` 和 `<body>` 部分中。


以 [JSON-LD](https://json-ld.org/) 结构化数据为例，在页面的 head 元素中使用 type 为 `application/ld+json` 的 script 元素，配置对应的信息，下面的例子是食谱网页上，描述了食谱的标题、作者和其他详情

![seo-google-json-ld.png](/images/seo/seo-google-json-ld.png)

```html

<html>
  <head>
    <title>Party Coffee Cake</title>
    <!-- "@type": "Recipe", 食谱类型 -->
    <script type="application/ld+json">
    {
      "@context": "https://schema.org/",
      "@type": "Recipe",
      "name": "Apple Pie by Grandma",
      "author": "Elaine Smith",
      "image": "https://images.edge-generalmills.com/56459281-6fe6-4d9d-984f-385c9488d824.jpg",
      "description": "A classic apple pie.",
      "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "4.8",
        "reviewCount": "7462",
        "bestRating": "5",
        "worstRating": "1"
      },
      "prepTime": "PT30M",
      "totalTime": "PT1H30M",
      "recipeYield": "8",
      "nutrition": {
        "@type": "NutritionInformation",
        "calories": "512 calories"
      },
      "recipeIngredient": [
        "1 box refrigerated pie crusts, softened as directed on box",
        "6 cups thinly sliced, peeled apples (6 medium)"
      ]
    }
    </script>
  </head>
</html>
```

### 具体配置详解

关于具体怎么一步一步写配置，参考 [Add structured data to your web pages](https://codelabs.developers.google.com/codelabs/structured-data#0)


```js
{
  // 告诉 Google 你正在使用 schema.org 结构化数据。schema.org 是一种结构化数据的模式。
  "@context": "http://schema.org/", 
  // 告诉谷歌你描述的是什么类型的东西，@type 为 Recipe（食谱）
  "@type": "Recipe",
  // 添加必需和推荐的属性，每种结构化数据类型都支持一系列属性，下面是食谱数据的菜名(name)配置
  "name": "Party Coffee Cake"
  // ....
}
```

具体配置属性介绍参考：[食谱（Recipe、HowTo、ItemList）结构化数据 | Google 搜索中心](https://developers.google.com/search/docs/appearance/structured-data/recipe?hl=zh-cn#recipe_properties)

除了食谱类型，还有很多其他类型的结构化数据，参见：[所有结构化数据类型](https://developers.google.com/search/docs/appearance/structured-data/search-gallery?hl=zh-cn)

![google-seo-struct-type-1.png](/images/seo/google-seo-struct-type-1.png)

![google-seo-struct-type-2.png](/images/seo/google-seo-struct-type-2.png)

### 验证、测试富媒体配置是否正确

[富媒体搜索结果测试 - Google Search Console](https://search.google.com/test/rich-results?hl=zh-cn)

以 FAQ 问答结构化数据为例，写了一个测试页面， http://zuo11.com/google/test-a.html。如果没有已上线的网也，也可以切换 tab 选择直接测试配置代码

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>google搜索富媒体测试 - (FAQ)</title>
    <script type="application/ld+json">
        {
          "@context": "https://schema.org",
          "@type": "FAQPage",
          "mainEntity": [{
            "@type": "Question",
            "name": "前端学 Vue 还是 React 好?",
            "acceptedAnswer": {
              "@type": "Answer",
              "text": "根据项目需要。"
            }
          }, { "@type": "Question",
            "name": "现在用 Vue3 的多吗，Vue2 还需要学吗？",
            "acceptedAnswer": {
              "@type": "Answer",
              "text": "新项目可以无脑上 vue3+ts，一般很多旧项目都是 vue2，vue2 还是要学的。"
            }
          }, {
            "@type": "Question",
            "name": "测试google问答富媒体搜索结果",
            "acceptedAnswer": {
              "@type": "Answer",
              "text": "<p>我是答案</p><p>第二个p标签，换行</p>"
            }
          }]
        }
        </script>
</head>

<body>
    google搜索富媒体测试，右键查看源码，head script type="application/ld+json" 部分
</body>

</html>
```

测试结果

![google-struct-data-test-result.png](/images/seo/google-struct-data-test-result.png)

## google 搜索结果中下载应用

> 资料来源：gogole 搜索 google search result deeplink ios

- [Download App Link in Google Search Results - stackoverflow](https://stackoverflow.com/questions/41714107/download-app-link-in-google-search-results)
- [App Indexing on Google Search](https://support.google.com/googleplay/android-developer/answer/6041489?hl=en)
- [在 Google 搜索中显示 iOS 应用中的内容 - google 搜索中心](https://developers.google.com/search/blog/2015/05/surfacing-content-from-ios-apps-in?hl=zh-cn)

## google 搜索结果展示案例

站点链接 + 结构化数据(应用)/deeplink

![google搜索效果.jpg](/images/seo/google搜索效果.jpg)