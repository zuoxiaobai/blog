---
outline: deep
---

# 前端动效

## nice-func 开源项目

尝试实现一些让人耳目一新、感觉很 nice 的网页内容、动效 [nice.zuo11.com](http://nice.zuo11.com)

完整代码地址：<https://github.com/dev-zuo/nice-func> 欢迎 Star、Fork

- 苹果官网 AirPods 充电盒动效实现，根据滚动位置打开、收起、旋转等
- iPhone14 新配色官网首屏动效实现
- OPPO Find X3 火星版官网：使用 GSAP 和 clip-path 实现 5 种场景切换动效
- MacBook Pro 新品发布官网动画效果实现(2023年01月)
- vivo iQOO Neo7 向下滚动切换手机颜色效果

## 常见动效

### 鼠标点击礼花效果

效果如下

![mouse-click-animation.gif](/images/animation/mouse-click-animation.gif)

实现原理：

1. 点击后，在 canvas 上在鼠标周围坐标随机绘制彩色圆形。
2. 动画效果实现有点巧妙，利用 requestAnimationFrame 来循环绘制圆形，每次绘制后，将每个圆的 x , y 设置为朝着四周散开的方向变更，圆半径逐渐减小。多次绘制后，半径 < 0 或者超过边界，就移除。
3. requestAnimationFrame 回调函数执行一次是 1 帧，如果屏幕刷新率为 60 hz，一秒执行 60 次，大概 1000ms/60 = 16.6ms 执行一次，循环里面逐渐更新 canvas 绘制的圆 x,y,r 就是比较平滑的动画了。

![mouse-click-boom.jpg](/images/animation/mouse-click-boom.jpg)

代码来源：<https://www.scczz.com/675.html>

完整代码：[鼠标点击礼花效果 - github](https://github.com/dev-zuo/fedemo/tree/master/src/DebugDemo/%E9%BC%A0%E6%A0%87%E7%82%B9%E5%87%BB%E7%A4%BC%E8%8A%B1%E6%95%88%E6%9E%9C)

## 3D 动画

[spline](https://spline.design/)，3D Design tool in the browser with real-time collaboration

[spline-viewer](https://viewer.spline.design/)，Easily embed your 3d Spline scenes into your website.

- 网页 3D 动画：[Spline 教程 | 一键导出SplineViewer代码，嵌入前端网页保姆式教程，含植物生长动画等案例 - bilibili](https://www.bilibili.com/video/BV1pm4y127kH)
- [B站最简单易上手的建模软件Spline教程 - GenJi是真想教会你](https://www.bilibili.com/video/BV14g411X73g)
- [Spline教程 | 教你零代码上线一个3D网站，Spline+Framer双厨狂喜（内含上线网站demo）](https://www.bilibili.com/video/BV1zW4y1t7EA/)

## TODO 动效

- 钉钉官网动效
- <https://kprverse.com/>
- <https://qu.ai/>
- <https://2019.makemepulse.com/>  WebGL，实现框架未知？
- <https://www.samuelsiebler.com/>

## 前端动效交互技术

TODO
