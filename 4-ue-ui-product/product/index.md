---
outline: deep
---

# 怎么做好一个产品

## 乔布斯聊苹果的定位、核心价值观

视频：[常看常新 - 乔布斯聊苹果的定位、核心价值观](https://twitter.com/DJWZ/status/1662304359563284480)

Our customers want to know who is Apple and what is it that we stand for. where do we fit in this world? what we're about isn't marking boxes for people to get their jobs done. although we do that well. we do that better than almost anybody, in some cases

我们的客户想知道苹果是谁，我们代表什么？我们在这个世界中的定位是什么？我们不仅仅是个帮人们完成工作的 "盒子"(机器)，虽然我们确实干这个，还干的不错，我们干这个几乎比谁都强，在某些方面

But Apple is about something more than that. Apple at the core... it's core value，is that, we believe that people with passion can change the world for the better. That's what we believe! And we have had the opportunity to work with people like that. we've had the opportunity to work with people like you.

但 apple 应该远不止是这个，苹果的核心价值观是，是我们相信，有激情的人们能改变世界，使其变得更好。这是我们相信的。并且我们有机会去和这样的人一起工作。我们有机会去和你们这样的你们一起合作。

with software developers, with customers, who have done it. In some big, and some small ways. And we believe that, id this world, people can change it for the better.And that those people who are crazy enough to think that they can change the world, are the ones that actually do!

和软件开发者、和客户们，一起实现这个，以各种或大或小的方式，并且我们相信，在这个世界，我们可以做到这一点。并且那些足够疯狂到觉得自己能改变世界的人，才是真正能够实现改变世界的人。

## 定价问题

![how-to-price.jpg](/images/product/how-to-price.jpg)

想要测试您的定价策略？ 🏆 创建三个等级的付费方式，但加入一些附加功能。

如果中间选项最受欢迎，继续提高价格，直到第一个变得更受欢迎。然后，重复之前的操作，以便中间的付费方式重新流行。

恭喜，您找到了完美的价格点！

参考：[Want to test your pricing strategy? - Danny Postma](https://twitter.com/dannypostmaa/status/1664438300625756160)
