# 用户体验/交互设计

## 网页体验

如果您对以下问题的回答为是，那么您很可能能够提供良好的网页体验：

- 网页是否具有良好的核心 Web 指标？
- 网页是否以安全的方式提供？
- 在移动设备上查看时，内容的显示效果是否良好？
- 内容是否不含过多会分散用户注意力或干扰主要内容的广告？
- 网页是否不含干扰性插页式广告？
- 访问者导航到或找到您的网页的主要内容的难易程度如何？
- 网页是否经过精心设计，使访问者可以轻松将其主要内容与网页上的其他内容区分开来？

## 网页体验优化

下面这些资源有助于您衡量、监控和优化网页体验：

- [了解核心 Web 指标和 Google 搜索结果](https://developers.google.com/search/docs/appearance/core-web-vitals?hl=zh-cn)：详细了解核心 Web 指标，包括 Search Console 中是如何报告这些指标的，以及有关如何改进网页的资源。
  - Largest Contentful Paint (LCP)：衡量加载性能。为了提供良好的用户体验，请尽力在网页开始加载的 2.5 秒内完成 LCP。
  - First Input Delay (FID)：衡量互动性。为了提供良好的用户体验，请尽力将 FID 控制在 100 毫秒以内。 从 2024 年 3 月开始，Interaction to Next Paint (INP) 将取代 FID 成为核心 Web 指标。
  - Cumulative Layout Shift (CLS)：衡量视觉稳定性。为了提供良好的用户体验，请尽力使 CLS 得分低于 0.1。

- [避免使用干扰性插页式广告和对话框](https://developers.google.com/search/docs/appearance/avoid-intrusive-interstitials?hl=zh-cn)：了解如何避免使用会导致内容不易访问的插页式广告。
- [Chrome Lighthouse](https://developer.chrome.com/docs/lighthouse/overview/)：Chrome 中的此工具集可帮助您确定与网页体验相关的一系列改进，包括在移动设备上的易用性。

参考：[了解 Google 搜索结果中的网页体验 - google](https://developers.google.com/search/docs/appearance/page-experience?hl=zh-cn)

## 动效对点击率提升的真实案列

推荐反复阅读：[我在淘宝做弹窗，2022 年初的回顾与展望 - 掘金](https://juejin.cn/post/7122797194519773221)

> 2021 年双十一购物车开奖提醒弹窗：该弹窗本来采用静态图片方式呈现后，上线后发现点击数据不佳，后决定添加摇一摇动效，由于该动效在之前的业务中已沉淀为可复用的动画模块，因此此次迭代只需数分钟便可完成上线。动效弹窗上线后，促使弹窗点击率提升 3.6 个百分点。

## UI 交互

### 多行文本输入

TODO
