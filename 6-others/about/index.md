# About

一名普通前端开发
<!-- 编译成一个按钮(文案有 alt 指定)，点击后显示图片 -->
<!-- <vImageViewer src="/images/互联网技术.png" alt="互联网技术" :inline="true"/> -->

## 技术交流

技术交流、合作 +v

![技术交流](/images/about/wechat.png)

## 在线技术支持

加入后，微信联系，有啥问题，手把手教，实在不会帮你写。

支持微信、腾讯会议 1v1 远程交流。

![zhishixingqiu](/images/about/zhishixingqiu.png)
