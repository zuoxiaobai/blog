---
outline: deep
---

# 实用工具

## AI 调色工具

- [khroma.co](https://www.khroma.co/)，做设计、做开发、穿衣服、家庭装饰，不会搭配颜色，试试选择 50 种自己喜欢的颜色，AI 按照选中颜色，计算出你个人专属调色板。

## 大麦网自动购票

[dm-ticket](https://github.com/ClassmateLin/dm-ticket)，大麦网自动购票, 支持docker一键部署。Damai automatically purchases tickets, running in docker container.

## mac bilibili 直播工具

- [obs](https://obsproject.com/) 录屏/直播推流
- obs 播放 QQ 音乐/网易云音乐
  - [LoopBack Audio](https://rogueamoeba.com/loopback/)，免费+收费，每隔 20 分钟会断开，需要重新关闭后再打开
  - [background-music](https://github.com/kyleneideck/BackgroundMusic)
- mac bilibili 弹幕 [bilive-danmaku - github](https://github.com/Beats0/bilive-danmaku)

参考：[MacOS 版 OBS 桌面音频禁用是怎么回事?](https://www.zhihu.com/question/377714519/answer/2623897362?utm_id=0)

## 在线代码演示、编辑器

### jsfiddle

例如: [Vue 2.0 Hello World](https://jsfiddle.net/50wL7mdz/40809)

### codepen

## win/mac 系统高层级倒计时工具

比如定 2h，提醒自己合理利用时间。类似 iPhone 倒计时样式

- electron-countdown 使用 Electron 制作的 Windows 倒计时应用
  - 下载 [electron-countdown 下载地址 - github](https://github.com/QiYuOr2/electron-countdown/releases/tag/1.0.0)
