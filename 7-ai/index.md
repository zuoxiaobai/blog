---
outline: deep
---

# AI 技术探索

## 传统开发与借助 AI 开发对比案例

来源： [AI 为产品开发带来的变化 - 倪爽 twitter](https://twitter.com/nishuang/status/1664716927069978625)

一个开发 MVP 产品的真实例子，客户雇了两个程序员

- Alex 来自德国，纯编程，19年经验
- Hamid 来自巴基斯坦，编程 + Copilot + GPT-4 + no-code，4年经验

对比两个人的开发速度、完成开发工作量、测试工作量、后期运维成本

### Hamid（AI + 无代码）

Hamid 在一周内完成了第一个版本，代码测试覆盖率达到100%，无代码部分的端到端测试。95%的工作似乎已经完成，乍一看似乎有效......

Hamid 在 @bubble 中构建了 UI 和前端工作流，使用 GPT-4 生成 Cloudflare Workers，使用 Copilot 集成现有代码，并使用 GPT-4（playwright/ava）生成测试

```bash
Hamid's costs:

GPT-4: $211
Copilot: $20
Cloudflare: $5
Bubble: $134
Compensation: $2460 (41 hours worked)

Costs to host/run: $139/mo
```

### Alex 纯编程

Alex 完成了大约 7% 的任务。估计所有费用：$45k。预计 $11k 用于添加测试。托管/运行成本：$ 20 /月

```bash
Alex finished around 7% of the tasks. Costs:

Vercel: $20
Compensation: $3500

Estimated to developing everything: $45k. And expects $11k for adding tests.

Costs to host/run: $20/mo
```

### 开发成本，效率问题

核心问题在于开发效率，怎么高质量快速搬砖？**积累可复用代码 ctrl c/v + 提效工具**

![code-vs-ai.png](/images/ai/code-vs-ai.png)

## chatgpt

### 调用 OpenAI 接口实现 chatgpt

Vue3 + Node.js 调用 OpenAI 接口实现 Chat、生成图片

代码地址：[chatgpt-node-vue3 - github](https://github.com/dev-zuo/chatgpt-node-vue3)

### chatgpt 流式输出 SSE 实现

什么是 SSE ？推荐看 [Server-Sent Events 教程 - 阮一峰](https://www.ruanyifeng.com/blog/2017/05/server-sent_events.html)

chatgpt stream 实时输出功能实现原理 SSE(Server-send events)

代码地址：[60 行代码 demo 理解 SSE（Server-sent events） 使用 - github](https://github.com/dev-zuo/fedemo/tree/master/src/DebugDemo/server-send-events)

### SSE 请求怎么支持 POST

一般 SSE 需要使用 EventSource API 发起，会自动发送 GET 请求，不需要使用 xhr，如果一定要使用 post

1、改为 socket 收发数据

2、分两次完成请求，先用 post 发送敏感数据，后端临时存储，再用 EventSource 发 sse get 请求用于接收数据

3、使用微软的库，[fetch-event-source - Azure](https://github.com/Azure/fetch-event-source)，不知道有没有坑

### 网页版 chatgpt 断线，打不开

对科学上网工具要求较高，可能你的梯子不够好，但也并不是越贵越好，不要盲目去买贵的。最优解：**问那些用的很流畅的人是怎么使用的**

相关资料: [KeepChatGPT - github](https://github.com/xcanwin/KeepChatGPT)，ChatGPT的畅聊与增强插件。开源免费。不仅能解决所有报错不再刷新，还有保持活跃、取消审计、克隆对话、净化首页、展示大屏、展示全屏、言无不尽、拦截跟踪、日新月异等多个高级功能。让我们的AI体验无比顺畅、丝滑、高效、简洁。

### OpenAI API 429 异常

免费额度用完，或者过期

### 免费额度用完后，怎么继续使用 API

1、付费

2、非官方 API [chatgpt - npm](https://www.npmjs.com/package/chatgpt) ,支持 ChatGPTUnofficialProxyAPI

3、白嫖，比如 [freegpt](https://freegpt.one/)，更多参考 [ChatGPT网址导航，分享免费好用AI网站！](https://github.com/LangLangShanDeNanKe/chatgpt)

### chatgpt 不支持国内支付怎么处理？

- 注册欧易交易平台账号，并完成USDT充值 => 申请 Depay 美国虚拟信用卡 => 支付，参考：[亲测有效 国内购买 ChatGPT Plus 订阅的方法教程](https://mfslab.com/23.html)
- 相关经验：[OpenAI/ChatGPT Plus信用卡绑定总结，整理了部分支持的信用卡 - 掘金](https://juejin.cn/post/7215032434353831994)

## 通过 AI 设计的 UI 收集

[Alien Interfaces](https://alieninterfaces.com/) 使用 AI 设计和构建的交互式界面的集合。通过 Midjourney 生成设计稿，然后实现，视频、在线示例：<https://alieninterfaces.com/cases/pikapika.html>

Midjourney + [Dreamer Figma Plug-in](https://www.figma.com/community/plugin/1151245850609894407/Dreamer)

![ai-ui.png](/images/ai/ai-ui.png)

## 声音拟合

怎么 copy 一个人的声音？

### AI 孙燕姿

## 视频生成处理

### AI 数字人

### 实时换脸

[DeepFaceLive - github](https://github.com/iperov/DeepFaceLive) 用于 PC 流媒体或视频通话的实时换脸 (Real-time face swap for PC streaming or video calls)

### 69 岁，马保国换脸实践

### 家人们谁懂啊，今天遇到一个虾头男

特效视频怎么生成的？

### p*****b 视频换脸实践
