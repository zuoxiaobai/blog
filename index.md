---
layout: home

hero:
  name: dev-zuo
  text: 知识体系，技能树
  tagline: 专注全栈开发、用户体验、交互设计、性能优化等
  image:
    # src: /dev.svg
    src: /dev.png
    alt: 前端开发
  actions:
    - theme: brand
      text: 快速开始
      link: /0-guide/
    - theme: alt
      text: About
      link: /6-others/about/

features:
  - icon: 🔥
    title: 前端开发
    details: 前端开发工程师相关技能梳理、知识整理（初级、中级、高级）。思维导图方式记录、实现结构化、体系化、树形成长
  - icon: 🍉
    title: 服务端开发
    details: 暂时以 Node.js（Koa）为主、涉及接口实现、运维部署、nginx/docker/CI、CD 等
  - icon: ✍
    title: 开发经验沉淀
    details: 开发经验整理、踩坑记录，代码沉淀、可复用代码/样式等收集，实现高效开发
  - icon: 🍋
    title: UI/交互设计/用户体验
    details: 什么是好的用户体验？追求更好的视觉效果、更好流畅的用户体验
  - icon: 🍊
    title: 计算机基础(cs)
    details: 数据结构与算法、leetcode 刷题记录、设计模式、HTTP、Git、单元测试等
  - icon: 🌈
    title: 英语
    details: 英语相关学习、语法、听力、写作等
---